package in.nareshit.ketan.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.nareshit.ketan.entity.Customer;
import in.nareshit.ketan.service.ICustomerService;
import in.nareshit.ketan.service.exception.CustomerNotFoundException;

@RestController
@RequestMapping("/customer")
public class CustomerRestController {
	@Autowired
	private ICustomerService service;
	
	//1 save Customer
	@PostMapping("/create")
	public ResponseEntity<String> createCustomer(
			@RequestBody Customer customer
			){
		ResponseEntity<String> response=null;
		
		Long id=service.saveCustomer(customer);
		response =ResponseEntity.ok("Customer'"+id+"' is created");
		
	return response;	
	}
	
	//2 fetching all customer
	@GetMapping("/all")
	public ResponseEntity<List<Customer>> getAllCustomers(){
		ResponseEntity<List<Customer>> response=null;
		
		List<Customer> list=service.getAllCustomers();
		response=ResponseEntity.ok(list);
		
		return response;
		
	}
	
	//3.Fetching customer by email
	@GetMapping("/find/{mail}")
    public ResponseEntity<Customer> getCustomerByEmail(
    		@PathVariable String mail
    		){
		ResponseEntity<Customer> response=null;
		try {
		Customer cust=	service.getOneCustomerByEmail(mail);
		response=new ResponseEntity<Customer>(cust,HttpStatus.OK);
			
		} catch (CustomerNotFoundException cnfe) {
			cnfe.printStackTrace();
			throw cnfe;
		}
		
		return response;
		
		//4.Fetching customer by AAdhar
	    //5.Fetching Customer by Mobile
		//6.Fetching Customer by PanCard
		
		
   }
	
	
	
	
	
	
	
	
	
	

}
