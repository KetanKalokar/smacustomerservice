package in.nareshit.ketan.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nareshit.ketan.entity.Customer;

public interface CustomerRepo extends JpaRepository<Customer, Long> {

	Optional<Customer> findByEmail(String email);
	Optional<Customer> findByAadharId(String aadharId);
	Optional<Customer> findByPancardId(String pancardId);
	Optional<Customer> findByMobile(String mobile);
	
}
