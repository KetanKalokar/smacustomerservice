package in.nareshit.ketan.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="cust_tab")
@Data
public class Customer {
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column(name="cust_id_col")
	private Long id;
	
	@Column(name="cust_name_col")
    private String name;
	
	@Column(name="cust_email_col")
	private String email;

	// Good Bye
	
	@Column(name="cust_gender_col")
	private String gender;
	
	@Column(name="cust_imgPath_col")
	private String imagePath;
    
	@Column(name="cust_mobile_col")
	private String mobile;
	
	@Column(name="cust_Addr_col")
	private String address;
	
	@Column(name="cust_panId_col")
	private String pancardId;
	
	@Column(name="cust_panId_col")
    private String aadharId;
	
	
	
	

}
