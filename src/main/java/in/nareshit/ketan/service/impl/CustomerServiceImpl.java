package in.nareshit.ketan.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import in.nareshit.ketan.entity.Customer;
import in.nareshit.ketan.repo.CustomerRepo;
import in.nareshit.ketan.service.ICustomerService;
import in.nareshit.ketan.service.exception.CustomerNotFoundException;

@Service
public class CustomerServiceImpl implements ICustomerService {
	
	private CustomerRepo repo;

	@Override
	public Long saveCustomer(Customer cust) {
		
		return repo.save(cust).getId() ;
	}

	@Override
	public Customer getOneCustomer(Long id) {
		Optional<Customer> opt=repo.findById(id);
		if(opt.isEmpty()) {
			throw new CustomerNotFoundException("Customer '"+id+"' not Found");
		}
		return opt.get();
	}

	@Override
	public Customer getOneCustomerByEmail(String email) {
		
		Optional<Customer> opt=repo.findByEmail(email);
		return validateInput(opt, email);
	}

	@Override
	public Customer getOneCustomerByPanCard(String pancard) {
		
		Optional<Customer> opt=repo.findByPancardId(pancard);
		return validateInput(opt, pancard);
	}

	@Override
	public Customer getOneCustomerByMobile(String mobile) {
		
		Optional<Customer> opt=repo.findByMobile(mobile);
		return validateInput(opt, mobile);
	}

	@Override
	public Customer getOneCustomerByAadhar(String aadhar) {
		
		Optional<Customer> opt=repo.findByAadharId(aadhar);
		 return validateInput(opt, aadhar);
	}

	@Override
	public List<Customer> getAllCustomers() {
		
		return repo.findAll();
	}
	
	private Customer validateInput(Optional<Customer> opt, String input) {
		/*
		 * if(opt.isEmpty()) {
		 *  throw new CustomerNotFoundException("Customer '"+input+"' not Found"); 
		 *  } 
		 *  return opt.get();
		 */
		 return opt.orElseThrow(
				 ()-> new CustomerNotFoundException("Customer '"+input+"' not Found") );
		
}
	
}
