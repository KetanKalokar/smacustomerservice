package in.nareshit.ketan.service;

import java.util.List;

import in.nareshit.ketan.entity.Customer;

public interface ICustomerService {

	Long saveCustomer(Customer cust);
	Customer getOneCustomer(Long id);
	Customer getOneCustomerByEmail(String email);
	Customer getOneCustomerByPanCard(String pancard);
	Customer getOneCustomerByMobile(String mobile);
	Customer getOneCustomerByAadhar(String aadhar);

	List<Customer> getAllCustomers();

}
